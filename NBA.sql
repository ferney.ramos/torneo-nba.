CREATE TABLE entrenador (
	cod_en varchar(10),
	fecha_nac date,
	nomb_en varchar(50)

);
ALTER TABLE entrenador ADD CONSTRAINT key_entrenador PRIMARY KEY(cod_en);
CREATE TABLE division (
	cod_div varchar(10),
	nomb_div varchar(50)
);
ALTER TABLE division ADD CONSTRAINT key_div PRIMARY KEY(cod_div);
CREATE TABLE ciudad (
	cod_ciu varchar(10),
	nomb_ciu varchar(50),
	nomb_pais varchar(50)
);
ALTER TABLE ciudad ADD CONSTRAINT key_ciu PRIMARY KEY(cod_ciu);
CREATE TABLE equipo (
	cod_equ varchar(10),
	nomb_equ varchar(50),
	conferencia varchar(50),
	cod_ciu varchar(10),
	cod_div varchar(10),
	cod_en varchar(10),
	cod_est varchar(10)
);
ALTER TABLE equipo ADD CONSTRAINT key_equ PRIMARY KEY(cod_equ);
CREATE TABLE partido (
	cod_part varchar(10),
	fecha date,
	tipo varchar(50),
	p_visitante bigint,
	p_local bigint,
	cod_est varchar(10),
	local varchar(10),
	visitante varchar(10)
);
ALTER TABLE partido ADD CONSTRAINT key_part PRIMARY KEY(cod_part);
CREATE TABLE estadio (
	cod_est varchar(10),
	nomb_est varchar(50),
	aforo bigint
);
ALTER TABLE estadio ADD CONSTRAINT key_est PRIMARY KEY(cod_est);
CREATE TABLE jugador (
	cod_jug varchar(10),
	altura float,
	fecha_naci date,
	nom_jug varchar(50),
	peso float,
	cod_ciu varchar(10),
	cod_pos varchar(10)
);
ALTER TABLE jugador ADD CONSTRAINT key_jug PRIMARY KEY(cod_jug);
CREATE TABLE posiciones (
	cod_pos varchar(10),
	nomb_pos varchar(50)
);
ALTER TABLE posiciones ADD CONSTRAINT key_pos PRIMARY KEY(cod_pos);
CREATE TABLE contrato (
	cod_equ varchar(10),
	cod_jug varchar(10),
	fecha_inicio date,
	fecha_retiro date,
	numero int8
);
ALTER TABLE contrato ADD CONSTRAINT key_contrato PRIMARY KEY(cod_equ,cod_jug);
CREATE TABLE cestas (
	cod_part varchar(10),
	cod_jug varchar(10),
	minuto int8,
	descrip varchar(100)
);
ALTER TABLE cestas ADD CONSTRAINT key_cesta PRIMARY KEY(cod_part,cod_jug);
ALTER TABLE equipo ADD CONSTRAINT dirige FOREIGN KEY (cod_en) REFERENCES entrenador(cod_en) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE equipo ADD CONSTRAINT pertenece FOREIGN KEY (cod_div) REFERENCES division(cod_div) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE equipo ADD CONSTRAINT reside FOREIGN KEY (cod_ciu) REFERENCES ciudad(cod_ciu) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE equipo ADD CONSTRAINT entrena FOREIGN KEY (cod_est) REFERENCES estadio(cod_est) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE jugador ADD CONSTRAINT nacio FOREIGN KEY (cod_ciu) REFERENCES ciudad(cod_ciu) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE jugador ADD CONSTRAINT juega FOREIGN KEY (cod_pos) REFERENCES posiciones(cod_pos) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE contrato ADD CONSTRAINT fk_equ FOREIGN KEY (cod_equ) REFERENCES equipo(cod_equ) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE contrato ADD CONSTRAINT fk_jug FOREIGN KEY (cod_jug) REFERENCES jugador(cod_jug) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE cestas ADD CONSTRAINT fk_part FOREIGN KEY (cod_part) REFERENCES partido(cod_part) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE cestas ADD CONSTRAINT fk_juga FOREIGN KEY (cod_jug) REFERENCES jugador(cod_jug) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE partido ADD CONSTRAINT programado FOREIGN KEY (cod_est) REFERENCES estadio(cod_est) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE partido ADD CONSTRAINT loc FOREIGN KEY (local) REFERENCES equipo(cod_equ) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE partido ADD CONSTRAINT visit FOREIGN KEY (visitante) REFERENCES equipo(cod_equ) ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE OR REPLACE FUNCTION transferir(new_equ,cod_juga) RETURNS TRIGGER AS $$
DECLARE
BEGIN

UPDATE contrato set cod_equ=new_equ, fecha_inicio=CURDATE(), fecha_retiro=DATE_ADD(NOW(),INTERVAL 5 YEAR) where cod_jug=cod_juga;

RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER transferencia BEFORE UPDATE 
ON contrato FOR EACH ROW
EXECUTE PROCEDURE transferir();

CREATE OR REPLACE FUNCTION llega(cod_equi,cod_juga,num) RETURNS TRIGGER AS $$
DECLARE
BEGIN

INSERT INTO contrato(cod_equ,cod_jug,fecha_inicio,fecha_retiro,numero) values(cod_equi,cod_juga,CURDATE(),DATE_ADD(NOW(),INTERVAL 5 YEAR),num);

RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo BEFORE INSERT 
ON jugador FOR EACH ROW
EXECUTE PROCEDURE llega(); 

