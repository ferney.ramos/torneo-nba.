<?php
        include 'app/Mostrar_Partido.php';
        $cod_part=$_GET["a"];
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Proyecto NBA</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/estilos.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Anton' rel='stylesheet'>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div  class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Este botón despliega la barra de navegación</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Proyecto NBA</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="#">Partidos</a></li>
                        <li><a href="#">Equipos</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Iniciar Sesión</a></li>
                        <li><a href="#">Registrarse</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="jumbotron">
                <h1>Proyecto NBA</h1>
                <p>
                    Acá van más weas del proyecto xdd
                </p>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Busqueda
                                </div>
                                <div class="panel-body">
                                    <form name="busqueda" class="" method="post" autocomplete="off" action="app/buscar_equipo.php">
                                        <div class="form-group">
                                            <input type="search" name="pb" class="form-control" placeholder="¿Qué buscas?">
                                        </div>
                                        <input type="submit" name="" value="Buscar" style="position: relative;left: 130px;">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-heading">
                                    <span class="glyphicon glyphicon-filter" aria-hidden="true"></span> Filtro
                                </div>
                                <div class="panel-body">
                                    <form name="filtro" class="" method="post" autocomplete="off" action="app/filtro.php">
                                        <div>
                                            <h3 style="margin:auto">Divisiones</h3>
                                            <div class="form-check" style="display: inline;">
                                                <input class="form-check-input" type="checkbox" name="check[]" id="Checkbox1" value="1">
                                                <label class="form-check-label" for="Checkbox1">Atlantico</label>
                                            </div>
                                            <div class="form-check" style="display: inline;">
                                                <input class="form-check-input" type="checkbox" name="check[]" id="Checkbox2" value="2">
                                                <label class="form-check-label" for="Checkbox2">Central</label>
                                            </div>
                                            <div class="form-check" style="display: inline;">
                                                <input class="form-check-input" type="checkbox" name="check[]" id="Checkbox3" value="3">
                                                <label class="form-check-label" for="Checkbox3">Noroeste</label>
                                            </div>
                                            <div class="form-check" style="display: inline;">
                                                <input class="form-check-input" type="checkbox" name="check[]" id="Checkbox4" value="4">
                                                <label class="form-check-label" for="Checkbox4">Pacifico</label>
                                            </div>
                                            <div class="form-check" style="display: inline-block;">
                                                <input class="form-check-input" type="checkbox" name="check[]" id="Checkbox5" value="5">
                                                <label class="form-check-label" for="Checkbox5">Sureste</label>
                                            </div>
                                            <div class="form-check" style="display: inline;">
                                                <input class="form-check-input" type="checkbox" name="check[]" id="Checkbox6" value="6">
                                                <label class="form-check-label" for="Checkbox6">Suroeste</label>
                                            </div>
                                        </div> 
                                        <div>
                                            <h3 style="margin:auto">Conferencia</h3>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="conf" id="exampleRadios1" value="1" checked>
                                                <label class="form-check-label" for="exampleRadios1">
                                                    Este
                                                </label>
                                                </div>
                                                <div class="form-check">
                                                <input class="form-check-input" type="radio" name="conf" id="exampleRadios2" value="2">
                                                <label class="form-check-label" for="exampleRadios2">
                                                    Oeste
                                                </label>
                                            </div>
                                        </div>
                                        <input type="submit" name="" value="Filtrar" style="position: relative;left: 130px;">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span><?php
                                $r=nomb_equ($cod_part);
                            ?>
                        </div>
                        <div class="panel-body">
                            <?php
                                $r=mostrar_partido_2($cod_part);
                            ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>    
    </body>
</html>    