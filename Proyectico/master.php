<?php
        include 'app/Mostrar_Partido.php';
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Proyecto NBA</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/estilos.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Anton' rel='stylesheet'>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div  class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Este botón despliega la barra de navegación</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Proyecto NBA</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="#">Partidos</a></li>
                        <li><a href="#">Equipos</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Iniciar Sesión</a></li>
                        <li><a href="#">Registrarse</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="jumbotron">
                <h1>Proyecto NBA</h1>
                <p>
                    Acá van más weas del proyecto xdd
                </p>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Crear jugador
                                </div>
                                <div class="panel-body">
                                    <form name="busqueda" class="" method="post" autocomplete="off" action="app/crear_jugador.php">
                                        <div class="form-group">
                                            <label for="a1">Nombre</label>
                                            <input type="text" class="form-control" id="a1" name="Nom_Jug">
                                        </div>
                                        <div class="form-group">
                                            <label for="a2">Altura</label>
                                            <input type="number" class="form-control" id="a2" name="Alt">
                                        </div>
                                        <div class="form-group">
                                            <label for="start">Fecha de nacimiento</label>
                                            <input type="date" id="start" name="fecha" value="2018-07-22" min="1950-01-01" max="2021-12-31">
                                        </div>
                                        <div class="form-group">
                                            <label for="a3">Peso</label>
                                            <input type="number" class="form-control" id="a3" name="Peso">
                                        </div>
                                        <div class="form-group">
                                            <label for="a4">Codigo ciudad</label>
                                            <input type="number" class="form-control" id="a4" name="Ciudad">
                                        </div>
                                        <div class="form-group">
                                            <label for="a5">Codigo posicion</label>
                                            <input type="number" class="form-control" min="1" max="5" id="a5" name="Posicion">
                                        </div>

                                        <input type="submit" name="" value="Buscar" style="position: relative;left: 130px;">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-heading">
                                    <span class="glyphicon glyphicon-filter" aria-hidden="true"></span> Filtro
                                </div>
                                <div class="panel-body">
                                    <form name="filtro" class="" method="post" autocomplete="off" action="app/borrar_jugador.php">
                                        <div class="form-group">
                                            <label for="a6">Codigo jugador</label>
                                            <input type="text" class="form-control" id="a6" name="cod_Jug">
                                        </div>
                                        <input type="submit" name="" value="Borrar" style="position: relative;left: 130px;">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span> Traspasar jugador
                        </div>
                        <div class="panel-body">
                            <div class="div_form">
                                <form name="form_contrato" class="" method="post" autocomplete="off" action="app/contrato.php">
                                    <div class="form-group">
                                        <label for="a1">Codigo del jugador</label>
                                        <input type="text" class="form-control" id="a1" name="Cod_Jug">
                                    </div>
                                    <div class="form-group">
                                        <label for="a2">Codigo del equipo a traspasar</label>
                                        <input type="text" class="form-control" id="a2" name="Cod_Equ">
                                    </div>
                                    <input type="submit" name="" value="Traspasar" style="position: relative;left: 325px;">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>    
    </body>
</html>    