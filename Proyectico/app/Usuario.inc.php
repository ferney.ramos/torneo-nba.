<?php

class User {

    private $id;
    private $name;
    private $email;
    private $password;
    private $registe_date;
    private $active;

    public function __construct($id, $name, $email, $password, $register_date, $active) {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->register_date = $register_date;
        $this->active = $active;
    }
    
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getEmail() {
        return $this->email;
    }

    function getPassword() {
        return $this->password;
    }

    function getRegiste_date() {
        return $this->registe_date;
    }

    function getActive() {
        return $this->active;
    }

    function setId($id): void {
        $this->id = $id;
    }

    function setName($name): void {
        $this->name = $name;
    }

    function setEmail($email): void {
        $this->email = $email;
    }

    function setPassword($password): void {
        $this->password = $password;
    }

    function setRegiste_date($registe_date): void {
        $this->registe_date = $registe_date;
    }

    function setActive($active): void {
        $this->active = $active;
    }



}
