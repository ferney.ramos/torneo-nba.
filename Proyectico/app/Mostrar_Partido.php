<?php
    function mostrar_partido($id){
        include 'Conexion.inc.php';
        $partido = pg_query("create view marcador as select cod_part,fecha,p_visitante,p_local,cod_est,local,visitante from partido where cod_part='".$id."'");
        $partido_arr= pg_fetch_row($partido, 0);
        $nomb_lol = pg_query("select nomb_equ from equipo where cod_equ='".$partido_arr[5]."'");
        $nomb_lol= pg_fetch_all_columns($nomb_lol, 0);
        $nomb_vos = pg_query("select nomb_equ from equipo where cod_equ='".$partido_arr[6]."'");
        $nomb_vos= pg_fetch_all_columns($nomb_vos, 0);
        $estadio= pg_query("select nomb_est from estadio where cod_est='".$partido_arr[4]."'");
        $estadio= pg_fetch_all_columns($estadio, 0);

       echo '
            <div class="div_partido" style="width:718px;height:118px;">
                <div class="div_izq" style="width:144px;height:100px;">
                    <center><h1>'.$nomb_lol[0].'</h1></center>
                </div>
                <div class="div_ptos" style="width:144px;height:100px;">
                    <h1 style="margin:auto">'.$partido_arr[3].'</h1>
                </div>
                <a href="partido.php?a='.$id.'">
                <div class="div_mid" style="width:125.46px;height:100px;">
                    <img src="img/vs.png" width=126.46px height=100px >
                </div>
                </a>
                <div class="div_ptos" style="width:144px;height:100px;">
                    <h1 style="margin:auto">'.$partido_arr[2].'</h1>
                </div>
                <div class="div_der" style="width:144px;height:100px;">
                    <center><h1>'.$nomb_vos[0].'</h1></center>
                </div>
                <div class="div_info" style="width:718px;height:18px;">
                    <p style="text-align: center;">Fecha: '.$partido_arr[1].' // Estadio: '.$estadio[0].'</p>
                </div>
            </div>
       ';
    }

    function mostrar_partido_2($id){
        include 'Conexion.inc.php';
        $partido = pg_query("select * from partido where cod_part='".$id."'");
        $partido_arr= pg_fetch_row($partido, 0);
        $nomb_lol = pg_query("select nomb_equ from equipo where cod_equ='".$partido_arr[5]."'");
        $nomb_lol= pg_fetch_all_columns($nomb_lol, 0);
        $nomb_vos = pg_query("select nomb_equ from equipo where cod_equ='".$partido_arr[6]."'");
        $nomb_vos= pg_fetch_all_columns($nomb_vos, 0);
        $estadio= pg_query("select nomb_est from estadio where cod_est='".$partido_arr[4]."'");
        $estadio= pg_fetch_all_columns($estadio, 0);
        echo '
            <div class="div_partido" style="width:718px;height:360px;">
                <div class="div_izq" style="width:144px;height:100px;">
                    <center><h1>'.$nomb_lol[0].'</h1></center>
                </div>
                <div class="div_ptos" style="width:144px;height:100px;">
                    <h1 style="margin:auto">'.$partido_arr[3].'</h1>
                </div>
                <div class="div_mid" style="width:126.46px;height:100px;">
                    <img src="img/vs.png" width=126.46px height=100px >
                </div>
                <div class="div_ptos" style="width:144px;height:100px;">
                    <h1 style="margin:auto">'.$partido_arr[2].'</h1>
                </div>
                <div class="div_der" style="width:144px;height:100px;">
                    <center><h1>'.$nomb_vos[0].'</h1></center>
                </div>
                <div class="div_cont" style="width:735px;height:260px;overflow-x:hidden;">
                    ';
                    
        echo ''.datos_partido($id).'
                </div>
                <div class="div_info" style="width:718px;height:18px;">
                    <p style="text-align: center;">Fecha: '.$partido_arr[1].' // Estadio: '.$estadio[0].'</p>
                </div>
            </div>
       ';
    }

    function div_partido(){
        include 'Conexion.inc.php';
        $partido = pg_query("select * from partido");
        $partido= pg_fetch_all_columns($partido, 0);
        for($y=0; $y<sizeof($partido); $y++){
            $notaf=mostrar_partido($partido[$y]);
        }
    }

    function div_partido_1($nomb_equ){
        include 'Conexion.inc.php';
        $aux=pg_query("select cod_equ from equipo where nomb_equ='".$nomb_equ."'");
        $aux= pg_fetch_row($aux, 0);
        $partido = pg_query("select * from partido where local='".$aux[0]."' or visitante='".$aux[0]."'");
        $partido= pg_fetch_all_columns($partido, 0);
        for($y=0; $y<sizeof($partido); $y++){
            $notaf=mostrar_partido($partido[$y]);
        }
    }

    function div_partido_2($div,$conf){
        include 'Conexion.inc.php';
        for($y=0; $y<sizeof($div); $y++){
            $equ=pg_query("select nomb_equ from equipo where cod_div='".$div[$y]."'");
            $equ= pg_fetch_all_columns($equ, 0);
            for($x=0; $x<sizeof($equ); $x++){
                $aux=div_partido_1($equ[$x]);
            }
        }
    }

    function nomb_equ($cod){
        include 'Conexion.inc.php';
        $cod=pg_query("select local,visitante from partido where cod_part='".$cod."'");
        $cod1= pg_fetch_all_columns($cod, 0);
        $cod2= pg_fetch_all_columns($cod,1);
        $cod1=pg_query("select nomb_equ from equipo where cod_equ='".$cod1[0]."'");
        $cod1=pg_fetch_all_columns($cod1,0);
        $cod2=pg_query("select nomb_equ from equipo where cod_equ='".$cod2[0]."'");
        $cod2=pg_fetch_all_columns($cod2,0);
        echo ''.$cod1[0].' VS '.$cod2[0].'';
    }

    function datos_partido($cod){
        include 'Conexion.inc.php';
        $info= pg_query("select cestas.*, equipo.cod_equ from cestas,jugador,equipo,contrato where cestas.cod_jug=jugador.cod_jug and contrato.cod_jug=jugador.cod_jug and contrato.cod_equ=equipo.cod_equ and cestas.cod_part='".$cod."' order by cestas.minuto");
        $aux=pg_fetch_all_columns($info,0);
        $eqi= pg_query("select local,visitante from partido where cod_part='".$cod."'");
        $eqi=pg_fetch_row($eqi,0);
        $i=sizeof($aux);
        for($x=0; $x < $i; $x++){
            $au1=pg_fetch_row($info,$x);
            div_datos_partido($au1,$eqi);
        }
    }

    function div_datos_partido($info,$eqi){
        $aux=pg_query("select jugador.nomb_jug, equipo.cod_equ from jugador,equipo,contrato where jugador.cod_jug=contrato.cod_jug and contrato.cod_equ=equipo.cod_equ and jugador.cod_jug='".$info[1]."'");
        $aux1=pg_fetch_all_columns($aux,0);
        $aux2=pg_fetch_all_columns($aux,1);
        if($eqi[0]==$aux2[0]){
            echo '
                <div class="div_izq" style="width:144px;height:100px;">
                    <center><h1>'.$aux1[0].'</h1></center>
                </div>
                <div class="div_ptos" style="width:144px;height:100px;">
                    <h1 style="margin:auto">'.$info[3].'</h1>
                </div>
                <div class="div_mid" style="width:126.46px;height:100px;position:relative;top:-36px;left:1px;">
                <h1 style="margin:auto">'.$info[2].'</h1>
                </div>
                <div class="div_ptos" style="width:144px;height:100px;top:-4px;">
                </div>
                <div class="div_der" style="width:144px;height:100px;top:0px;left:3px;">
                </div>
            ';
        }else{
            echo '
                <div class="div_izq" style="width:144px;height:100px;top:-1px">
                </div>
                <div class="div_ptos" style="width:144px;height:100px;">
                </div>
                <div class="div_mid" style="width:126.46px;height:100px;position:relative;top:-36px;left:1px;">
                    <h1 style="margin:auto">'.$info[2].'</h1>
                </div>
                <div class="div_ptos" style="width:144px;height:100px;top:-37px;">
                    <h1 style="margin:auto">'.$info[3].'</h1>
                </div>
                <div class="div_der" style="width:144px;height:100px;top:-45px;left:3px;">
                    <center><h1>'.$aux1[0].'</h1></center>
                </div>
            ';
        }
        
    }

?>